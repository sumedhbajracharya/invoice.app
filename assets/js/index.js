//initialization
let items = []
var grand;
var trow = document.getElementsByTagName("tr");
// splashScreen
function removeSplash() {
    setTimeout(() => document.getElementById('splash').remove(), 4600)
}
// addingItemsHandler
const addItem = () => {


    var modal = document.getElementById("myModal");

    var btn = document.getElementById("add-btn");

    var span = document.getElementById("close");


    let itemName = document.getElementById('item-name').value;
    let itemPrice = document.getElementById('item-price').value;
    let itemQty = document.getElementById('item-qty').value;
    let totalAmount = parseFloat(itemPrice * itemQty);
    if (itemName == 0 || itemPrice == 0 || itemQty == 0) {
        modal.style.display = "block";

        span.onclick = function () {
            modal.style.display = "none";
        }

        window.onclick = function (event) {
            if (event.target == modal) {
                modal.style.display = "none";
            }
        }
    } else if (isNaN(itemPrice) || isNaN(itemQty)) {
        modal.style.display = "block";

        span.onclick = function () {
            modal.style.display = "none";
        }

        window.onclick = function (event) {
            if (event.target == modal) {
                modal.style.display = "none";
            }
        }
    } else {
        items.push([itemName, itemPrice, itemQty, totalAmount]); //add item to array
        document.getElementById("control").reset();

        updateInvTable();
    }
}
//updatingTableHandler
const updateInvTable = () => {
    document.getElementById('invoice-tbody').innerHTML = "";
    var table = document.getElementById("invoice-tbody");
    grand = 0
    for (var i = 0; i < items.length; i++) {
        var row = table.insertRow(i);
        var cell1 = row.insertCell(0);
        var cell2 = row.insertCell(1);
        var cell3 = row.insertCell(2);
        var cell4 = row.insertCell(3);
        var cell5 = row.insertCell(4);

        cell1.innerHTML = "<input size='3' disabled id='name' type='text' value=" + items[i][0] + ">";
        cell2.innerHTML = "<input size='3' disabled id='price' type='text' value=" + items[i][1] + ">";
        cell3.innerHTML = "<input size='3' disabled id='qty' type='text' value=" + items[i][2] + ">";
        cell4.innerHTML = "<i class='fas fa-edit' id='edit-btn' onclick='editRow(this);'></i> <i class='fas fa-trash' id='dlt-btn' onclick='deleteRow(this);'></i>"
        cell5.innerHTML = items[i][3];
        grand += items[i][3];
    }
    rn = trow.length;
    cn = 4;
    var y = document.getElementById('invoice-table').rows[rn - 1].cells;
    y[1].innerHTML = grand.toFixed(2);
    return grand;
}
//grandTotalPostDiscount&VAT
const grandy = () => {

    var modal = document.getElementById("myModal");

    var btn = document.getElementById("add-btn");

    var span = document.getElementById("close");


    grand = 0;
    var grandfinal = 0;
    var disc = document.getElementById('disc-amt').value;
    var vat = document.getElementById('vat-rate').value;
    if (isNaN(disc) || isNaN(vat)) {
        modal.style.display = "block";

        span.onclick = function () {
            modal.style.display = "none";
        }

        window.onclick = function (event) {
            if (event.target == modal) {
                modal.style.display = "none";
            }
        }
        document.getElementById('disc-amt').value = '';
        document.getElementById('vat-rate').value = '';
    } else {
        for (var i = 0; i < items.length; i++) {
            grand += items[i][3];
        }
        rn = trow.length;
        cn = 4;
        var y = document.getElementById('invoice-table').rows[rn - 1].cells;
        grandfinal = (grand - disc) + ((vat / 100) * (grand - disc));

        y[1].innerHTML = grandfinal;

    }
}
//deleteRowHandler
const deleteRow = (element) => {
    const index = element.parentNode.parentNode.rowIndex;
    const invoiceTable = document.getElementById('invoice-table');
    invoiceTable.deleteRow(index);
    items.splice(index - 1, 1);
    grandy();
}
//editRowHandler
const editRow = (element) => {
    for (i = 0; i < 3; i++) {
        element.parentNode.parentNode.children[i].children[0].disabled = false;
    }
    const save = document.createElement('i');
    save.setAttribute('class', 'fa fa-check');
    save.setAttribute('onclick', 'saveEdit(this)');
    element.parentNode.appendChild(save);
    document.getElementById('edit-btn').remove()
}
//saveAfterEditHandler
const saveEdit = (element) => {
    const index = element.parentNode.parentNode.rowIndex;
    const name = element.parentNode.parentNode.children[0].children[0].value;
    const qty = element.parentNode.parentNode.children[1].children[0].value;
    const price = element.parentNode.parentNode.children[2].children[0].value;
    console.log(name + " " + qty + " " + price)
    let tempArr = items.slice(0, index - 1)
    tempArr.push([name, qty, price, qty * price]);
    tempArr = [...tempArr, ...items.slice(index)];
    items = tempArr;
    updateInvTable()
}

//print
function printDiv() {
    var divContents = document.getElementById("disc-amt").innerHTML;
    var a = window.open('', '', 'height=500, width=500');
    a.document.write('<html>');
    a.document.write('<body>to print <br>');
    a.document.write(divContents);
    a.document.write('</body></html>');
    a.document.close();
    a.print();
} 